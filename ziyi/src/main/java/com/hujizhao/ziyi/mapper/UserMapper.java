package com.hujizhao.ziyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hujizhao.ziyi.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
}