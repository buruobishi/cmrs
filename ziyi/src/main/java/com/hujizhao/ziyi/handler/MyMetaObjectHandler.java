package com.hujizhao.ziyi.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: hujizhao
 * Date: 2022/3/19
 * Time: 18:32
 * Description: No Description
 */

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    //mp执行添加操作时候这个方法添加
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
        this.setFieldValByName("version",1,metaObject);

    }

    //mp执行修改操作时候这个方法执行
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime",new Date(),metaObject);

    }
}
