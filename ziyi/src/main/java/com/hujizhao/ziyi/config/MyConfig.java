package com.hujizhao.ziyi.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * User: hujizhao
 * Date: 2022/3/19
 * Time: 21:18
 * Description: No Description
 */
@Configuration
@MapperScan("com.hujizhao.ziyi.mapper")
public class MyConfig {
    /**
     * 乐观锁插件
     * @return
     */
    /*
    报错：Parameter 'MP_OPTLOCK_VERSION_ORIGINAL' not found. Available parameters are [param1, et]
	新版本的 mybatisplus-plus 会出现这个问题,当我们根据官方文档使用乐观锁的相关代码时会出现这个问题:
    注意 发现这个拦截器OptimisticLockerInterceptor已经弃用，建议使用MybatisPlusInterceptor
     */
    @Bean
    public MybatisPlusInterceptor optimisticLockerInnerInterceptor(){
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return interceptor;
    }
    /**
     * 分页插件
     */
    @Bean
    public PaginationInnerInterceptor paginationInnerInterceptor(){
        return new PaginationInnerInterceptor();
    }



}
