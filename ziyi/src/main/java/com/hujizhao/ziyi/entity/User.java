package com.hujizhao.ziyi.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.util.Date;

@Data
public class User {
    /**
     * IdType
     * IdType.AUTO:数据库ID自增
     * IdType.ASSIGN_ID:如果不设置类型值，默认则使用IdType.ASSIGN_ID策略（自3.3.0起）。该策略会使用雪花算法自动生成主键ID，主键类型为长或字符串（分别对应的MySQL的表字段为BIGINT和VARCHAR）
     * IdType.ASSIGN_UUID:如果使用IdType.ASSIGN_UUID策略，并重新自动生成排除中划线的UUID作为主键。主键类型为String，对应MySQL的表分段为VARCHAR（32）
     * IdType.INPUT:自己输入id
     * IdType.NONE:NONE 该类型为未设置主键类型
     *
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    private String name;
    private Integer age;
    private String email;
    //乐观锁
    @Version
    @TableField(fill = FieldFill.INSERT)
    private Integer version;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    //添加和修改设置值
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}